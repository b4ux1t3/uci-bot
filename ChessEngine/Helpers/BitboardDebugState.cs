﻿namespace ChessEngine.Helpers
{
    public static class BitboardDebugState
    {
        public static bool BitboardDebugVisualizationRequested { get; set; }
        public static ulong BitboardToVisualize {get; set;}
    }
}
